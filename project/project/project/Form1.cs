﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SD = System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace project
{
    public partial class makeup : Form
    {
        public static double плата = 0, решта;
        public makeup()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nYSL Lipstick             UAH1250";
            плата = плата + 1250;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nDior Concealer           UAH1199";
            плата = плата + 1199;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nToo Faced MakeUp Kit     UAH5900";
            плата = плата + 5900;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nYSL Lip Gloss            UAH1300";
            плата = плата + 1300;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nHuda Beauty Eyeshadow    UAH1780";
            плата = плата + 1780;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            slipRtxb.Text = slipRtxb.Text + "\nYSL Foundation           UAH1400";
            плата = плата + 1400;
            dueLbl.Text = "UAH" + плата.ToString();
        }

        private void cashtbx_TextChanged(object sender, EventArgs e)
        {

        }

        private void nextCustBtn_Click(object sender, EventArgs e)
        {
            решта = 0;
            плата = 0;
            slipRtxb.Text = "";
            dueLbl.Text = "0";
            cashtbx.Text = "";
            changeLabel.Text = "0";
        }

        private void payBtn_Click_1(object sender, EventArgs e)
        {
            double готівка = Convert.ToDouble(cashtbx.Text);
            решта = готівка - плата;

            if (готівка < 0 )
                MessageBox.Show("Не достатньо коштів");

            changeLabel.Text = "UAH" + решта.ToString();


        }

        private void dueLbl_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(nameOfItem.SelectedItem.ToString() == "YSL Foundation")
            {
                if(typecombo.SelectedItem.ToString() == "Для шкіри")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 1400).ToString();

                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
                
            }
            else if (nameOfItem.SelectedItem.ToString() == "YSL Lipstick")
            {
                if(typecombo.SelectedItem.ToString() == "Для губ")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 1250).ToString();
                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
            }
            else if (nameOfItem.SelectedItem.ToString() == "YSL Lip Gloss")
            {
                if (typecombo.SelectedItem.ToString() == "Для губ")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 1300).ToString();
                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
            }
            else if (nameOfItem.SelectedItem.ToString() == "Dior Concealer")
            {
                if (typecombo.SelectedItem.ToString() == "Для шкіри")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 1199).ToString();
                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
            }
            else if (nameOfItem.SelectedItem.ToString() == "Huda Beauty Eyeshadow")
            {
                if (typecombo.SelectedItem.ToString() == "Для очей")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 1780).ToString();
                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
            }
            else if (nameOfItem.SelectedItem.ToString() == "Too Faced MakeUp Kit")
            {
                if (typecombo.SelectedItem.ToString() == "Для всього")
                {
                    paytext.Text = (float.Parse(quantitycombo.Text) * 5900).ToString();
                }
                dataGridView1.Rows.Add(idtext.Text, nameOfItem.Text, typecombo.Text, quantitycombo.Text, paytext.Text);
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            idtext.Text = "";
            nameOfItem.Text = "";
            typecombo.Text = "";
            quantitycombo.Text = "";
            paytext.Text = "";
        }

        private void idtext_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {

            }
            else
            {
                e.Handled = e.KeyChar != (Char)Keys.Back;
            }
        }

        private void export_Click(object sender, EventArgs e)
        {
            Excel.Application exApp = new Excel.Application();

            exApp.Workbooks.Add();
            Excel.Worksheet wsh = (Excel.Worksheet)exApp.ActiveSheet;
            
            int i, j;
            for(i=0; i <= dataGridView1.RowCount-2; i++)
            {
                for (j=0; j<=dataGridView1.ColumnCount-1; j++) 
                {
                    wsh.Cells[1, j + 1] = dataGridView1.Columns[j].HeaderText.ToString();
                    wsh.Cells[i+1, j + 1] = dataGridView1[j, i].Value.ToString();
                }
            }

            exApp.Visible = true;
        }

        private void makeup_Load(object sender, EventArgs e)
        {

        }
    }
}
