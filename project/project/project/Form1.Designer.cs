﻿namespace project
{
    partial class makeup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(makeup));
            this.YSL_Foundation = new System.Windows.Forms.PictureBox();
            this.YSL_Lipstick = new System.Windows.Forms.PictureBox();
            this.YSL_LipGloss = new System.Windows.Forms.PictureBox();
            this.TooFaced_MakeUpKit = new System.Windows.Forms.PictureBox();
            this.Dior_Concealer = new System.Windows.Forms.PictureBox();
            this.HudaBeauty_Eyeshadow = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.slipRtxb = new System.Windows.Forms.RichTextBox();
            this.nextCustBtn = new System.Windows.Forms.Button();
            this.cashtbx = new System.Windows.Forms.TextBox();
            this.changeLabel = new System.Windows.Forms.Label();
            this.dueLbl = new System.Windows.Forms.Label();
            this.payBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.idtext = new System.Windows.Forms.TextBox();
            this.nameOfItem = new System.Windows.Forms.ComboBox();
            this.typecombo = new System.Windows.Forms.ComboBox();
            this.quantitycombo = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.payment = new System.Windows.Forms.Label();
            this.paytext = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantitycolom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentcolom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.export = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.YSL_Foundation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YSL_Lipstick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YSL_LipGloss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TooFaced_MakeUpKit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dior_Concealer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HudaBeauty_Eyeshadow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // YSL_Foundation
            // 
            this.YSL_Foundation.Image = ((System.Drawing.Image)(resources.GetObject("YSL_Foundation.Image")));
            this.YSL_Foundation.Location = new System.Drawing.Point(4, 104);
            this.YSL_Foundation.Name = "YSL_Foundation";
            this.YSL_Foundation.Size = new System.Drawing.Size(242, 229);
            this.YSL_Foundation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YSL_Foundation.TabIndex = 0;
            this.YSL_Foundation.TabStop = false;
            this.YSL_Foundation.Tag = "makeupPbx_Click";
            this.YSL_Foundation.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // YSL_Lipstick
            // 
            this.YSL_Lipstick.Image = ((System.Drawing.Image)(resources.GetObject("YSL_Lipstick.Image")));
            this.YSL_Lipstick.Location = new System.Drawing.Point(252, 104);
            this.YSL_Lipstick.Name = "YSL_Lipstick";
            this.YSL_Lipstick.Size = new System.Drawing.Size(200, 148);
            this.YSL_Lipstick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YSL_Lipstick.TabIndex = 1;
            this.YSL_Lipstick.TabStop = false;
            this.YSL_Lipstick.Tag = "makeupPbx_Click";
            this.YSL_Lipstick.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // YSL_LipGloss
            // 
            this.YSL_LipGloss.Image = ((System.Drawing.Image)(resources.GetObject("YSL_LipGloss.Image")));
            this.YSL_LipGloss.Location = new System.Drawing.Point(436, 258);
            this.YSL_LipGloss.Name = "YSL_LipGloss";
            this.YSL_LipGloss.Size = new System.Drawing.Size(146, 75);
            this.YSL_LipGloss.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.YSL_LipGloss.TabIndex = 2;
            this.YSL_LipGloss.TabStop = false;
            this.YSL_LipGloss.Tag = "makeupPbx_Click";
            this.YSL_LipGloss.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // TooFaced_MakeUpKit
            // 
            this.TooFaced_MakeUpKit.Image = ((System.Drawing.Image)(resources.GetObject("TooFaced_MakeUpKit.Image")));
            this.TooFaced_MakeUpKit.Location = new System.Drawing.Point(458, 104);
            this.TooFaced_MakeUpKit.Name = "TooFaced_MakeUpKit";
            this.TooFaced_MakeUpKit.Size = new System.Drawing.Size(201, 148);
            this.TooFaced_MakeUpKit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.TooFaced_MakeUpKit.TabIndex = 3;
            this.TooFaced_MakeUpKit.TabStop = false;
            this.TooFaced_MakeUpKit.Tag = "makeupPbx_Click";
            this.TooFaced_MakeUpKit.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // Dior_Concealer
            // 
            this.Dior_Concealer.Image = ((System.Drawing.Image)(resources.GetObject("Dior_Concealer.Image")));
            this.Dior_Concealer.Location = new System.Drawing.Point(665, 104);
            this.Dior_Concealer.Name = "Dior_Concealer";
            this.Dior_Concealer.Size = new System.Drawing.Size(141, 229);
            this.Dior_Concealer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dior_Concealer.TabIndex = 4;
            this.Dior_Concealer.TabStop = false;
            this.Dior_Concealer.Tag = "makeupPbx_Click";
            this.Dior_Concealer.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // HudaBeauty_Eyeshadow
            // 
            this.HudaBeauty_Eyeshadow.Image = ((System.Drawing.Image)(resources.GetObject("HudaBeauty_Eyeshadow.Image")));
            this.HudaBeauty_Eyeshadow.Location = new System.Drawing.Point(252, 258);
            this.HudaBeauty_Eyeshadow.Name = "HudaBeauty_Eyeshadow";
            this.HudaBeauty_Eyeshadow.Size = new System.Drawing.Size(169, 75);
            this.HudaBeauty_Eyeshadow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.HudaBeauty_Eyeshadow.TabIndex = 5;
            this.HudaBeauty_Eyeshadow.TabStop = false;
            this.HudaBeauty_Eyeshadow.Tag = "makeupPbx_Click";
            this.HudaBeauty_Eyeshadow.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(868, 354);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 28);
            this.label1.TabIndex = 6;
            this.label1.Text = "Готівка:";
            // 
            // slipRtxb
            // 
            this.slipRtxb.BackColor = System.Drawing.Color.Black;
            this.slipRtxb.Font = new System.Drawing.Font("NSimSun", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slipRtxb.ForeColor = System.Drawing.Color.White;
            this.slipRtxb.Location = new System.Drawing.Point(868, 1);
            this.slipRtxb.Name = "slipRtxb";
            this.slipRtxb.Size = new System.Drawing.Size(451, 289);
            this.slipRtxb.TabIndex = 7;
            this.slipRtxb.Text = "";
            // 
            // nextCustBtn
            // 
            this.nextCustBtn.BackColor = System.Drawing.Color.MistyRose;
            this.nextCustBtn.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextCustBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.nextCustBtn.Location = new System.Drawing.Point(954, 458);
            this.nextCustBtn.Name = "nextCustBtn";
            this.nextCustBtn.Size = new System.Drawing.Size(218, 52);
            this.nextCustBtn.TabIndex = 8;
            this.nextCustBtn.Text = "Наступний клієнт";
            this.nextCustBtn.UseVisualStyleBackColor = false;
            this.nextCustBtn.Click += new System.EventHandler(this.nextCustBtn_Click);
            // 
            // cashtbx
            // 
            this.cashtbx.BackColor = System.Drawing.Color.Black;
            this.cashtbx.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cashtbx.ForeColor = System.Drawing.Color.White;
            this.cashtbx.Location = new System.Drawing.Point(954, 350);
            this.cashtbx.Name = "cashtbx";
            this.cashtbx.Size = new System.Drawing.Size(96, 36);
            this.cashtbx.TabIndex = 9;
            this.cashtbx.TextChanged += new System.EventHandler(this.cashtbx_TextChanged);
            // 
            // changeLabel
            // 
            this.changeLabel.AutoSize = true;
            this.changeLabel.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.changeLabel.Location = new System.Drawing.Point(868, 421);
            this.changeLabel.Name = "changeLabel";
            this.changeLabel.Size = new System.Drawing.Size(80, 28);
            this.changeLabel.TabIndex = 6;
            this.changeLabel.Text = "Решта:";
            // 
            // dueLbl
            // 
            this.dueLbl.AutoSize = true;
            this.dueLbl.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dueLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.dueLbl.Location = new System.Drawing.Point(868, 293);
            this.dueLbl.Name = "dueLbl";
            this.dueLbl.Size = new System.Drawing.Size(68, 28);
            this.dueLbl.TabIndex = 6;
            this.dueLbl.Text = "Сума:";
            this.dueLbl.Click += new System.EventHandler(this.dueLbl_Click);
            // 
            // payBtn
            // 
            this.payBtn.BackColor = System.Drawing.Color.MistyRose;
            this.payBtn.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.payBtn.Location = new System.Drawing.Point(1096, 350);
            this.payBtn.Name = "payBtn";
            this.payBtn.Size = new System.Drawing.Size(211, 36);
            this.payBtn.TabIndex = 10;
            this.payBtn.Text = "Розрахуватися";
            this.payBtn.UseVisualStyleBackColor = false;
            this.payBtn.Click += new System.EventHandler(this.payBtn_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Purple;
            this.label2.Location = new System.Drawing.Point(277, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(305, 97);
            this.label2.TabIndex = 11;
            this.label2.Text = "MakeUp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(-1, 369);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 28);
            this.label3.TabIndex = 12;
            this.label3.Text = "ID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(-1, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 28);
            this.label4.TabIndex = 13;
            this.label4.Text = "Назва товару:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(-1, 425);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 28);
            this.label5.TabIndex = 14;
            this.label5.Text = "Тип:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(-1, 453);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 28);
            this.label6.TabIndex = 15;
            this.label6.Text = "Кількість:";
            // 
            // idtext
            // 
            this.idtext.Location = new System.Drawing.Point(176, 374);
            this.idtext.Name = "idtext";
            this.idtext.Size = new System.Drawing.Size(245, 22);
            this.idtext.TabIndex = 16;
            this.idtext.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idtext_KeyPress);
            // 
            // nameOfItem
            // 
            this.nameOfItem.FormattingEnabled = true;
            this.nameOfItem.Items.AddRange(new object[] {
            "YSL Foundation",
            "YSL Lipstick",
            "YSL Lip Gloss",
            "Dior Concealer",
            "Huda Beauty Eyeshadow",
            "Too Faced MakeUp Kit"});
            this.nameOfItem.Location = new System.Drawing.Point(176, 401);
            this.nameOfItem.Name = "nameOfItem";
            this.nameOfItem.Size = new System.Drawing.Size(245, 24);
            this.nameOfItem.TabIndex = 20;
            // 
            // typecombo
            // 
            this.typecombo.FormattingEnabled = true;
            this.typecombo.Items.AddRange(new object[] {
            "Для шкіри",
            "Для очей",
            "Для губ",
            "Для всього"});
            this.typecombo.Location = new System.Drawing.Point(176, 427);
            this.typecombo.Name = "typecombo";
            this.typecombo.Size = new System.Drawing.Size(245, 24);
            this.typecombo.TabIndex = 21;
            // 
            // quantitycombo
            // 
            this.quantitycombo.FormattingEnabled = true;
            this.quantitycombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.quantitycombo.Location = new System.Drawing.Point(176, 456);
            this.quantitycombo.Name = "quantitycombo";
            this.quantitycombo.Size = new System.Drawing.Size(245, 24);
            this.quantitycombo.TabIndex = 22;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.name,
            this.type,
            this.quantitycolom,
            this.paymentcolom});
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.dataGridView1.Location = new System.Drawing.Point(4, 525);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(677, 213);
            this.dataGridView1.TabIndex = 23;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.MistyRose;
            this.button1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button1.Location = new System.Drawing.Point(4, 757);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(345, 60);
            this.button1.TabIndex = 24;
            this.button1.Text = "Уведіть";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.MistyRose;
            this.button2.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(363, 757);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(318, 60);
            this.button2.TabIndex = 25;
            this.button2.Text = "Очистити";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // payment
            // 
            this.payment.AutoSize = true;
            this.payment.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.payment.Location = new System.Drawing.Point(-1, 482);
            this.payment.Name = "payment";
            this.payment.Size = new System.Drawing.Size(89, 28);
            this.payment.TabIndex = 26;
            this.payment.Text = "Оплата:";
            // 
            // paytext
            // 
            this.paytext.Location = new System.Drawing.Point(176, 488);
            this.paytext.Name = "paytext";
            this.paytext.Size = new System.Drawing.Size(245, 22);
            this.paytext.TabIndex = 27;
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.Width = 125;
            // 
            // name
            // 
            this.name.HeaderText = "Назва товару";
            this.name.MinimumWidth = 6;
            this.name.Name = "name";
            this.name.Width = 125;
            // 
            // type
            // 
            this.type.HeaderText = "Тип";
            this.type.MinimumWidth = 6;
            this.type.Name = "type";
            this.type.Width = 125;
            // 
            // quantitycolom
            // 
            this.quantitycolom.HeaderText = "Кількість";
            this.quantitycolom.MinimumWidth = 6;
            this.quantitycolom.Name = "quantitycolom";
            this.quantitycolom.Width = 125;
            // 
            // paymentcolom
            // 
            this.paymentcolom.HeaderText = "Оплата";
            this.paymentcolom.MinimumWidth = 6;
            this.paymentcolom.Name = "paymentcolom";
            this.paymentcolom.Width = 125;
            // 
            // export
            // 
            this.export.BackColor = System.Drawing.Color.MistyRose;
            this.export.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.export.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.export.Location = new System.Drawing.Point(4, 823);
            this.export.Name = "export";
            this.export.Size = new System.Drawing.Size(345, 60);
            this.export.TabIndex = 28;
            this.export.Text = "Експорт";
            this.export.UseVisualStyleBackColor = false;
            this.export.Click += new System.EventHandler(this.export_Click);
            // 
            // makeup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1319, 899);
            this.Controls.Add(this.export);
            this.Controls.Add(this.paytext);
            this.Controls.Add(this.payment);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.quantitycombo);
            this.Controls.Add(this.typecombo);
            this.Controls.Add(this.nameOfItem);
            this.Controls.Add(this.idtext);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.payBtn);
            this.Controls.Add(this.cashtbx);
            this.Controls.Add(this.nextCustBtn);
            this.Controls.Add(this.slipRtxb);
            this.Controls.Add(this.changeLabel);
            this.Controls.Add(this.dueLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HudaBeauty_Eyeshadow);
            this.Controls.Add(this.Dior_Concealer);
            this.Controls.Add(this.TooFaced_MakeUpKit);
            this.Controls.Add(this.YSL_LipGloss);
            this.Controls.Add(this.YSL_Lipstick);
            this.Controls.Add(this.YSL_Foundation);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "makeup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MakeUp.ua";
            this.Load += new System.EventHandler(this.makeup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.YSL_Foundation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YSL_Lipstick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YSL_LipGloss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TooFaced_MakeUpKit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dior_Concealer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HudaBeauty_Eyeshadow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox YSL_Foundation;
        private System.Windows.Forms.PictureBox YSL_Lipstick;
        private System.Windows.Forms.PictureBox YSL_LipGloss;
        private System.Windows.Forms.PictureBox TooFaced_MakeUpKit;
        private System.Windows.Forms.PictureBox Dior_Concealer;
        private System.Windows.Forms.PictureBox HudaBeauty_Eyeshadow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox slipRtxb;
        private System.Windows.Forms.Button nextCustBtn;
        private System.Windows.Forms.TextBox cashtbx;
        private System.Windows.Forms.Label changeLabel;
        private System.Windows.Forms.Label dueLbl;
        private System.Windows.Forms.Button payBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idtext;
        private System.Windows.Forms.ComboBox nameOfItem;
        private System.Windows.Forms.ComboBox typecombo;
        private System.Windows.Forms.ComboBox quantitycombo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label payment;
        private System.Windows.Forms.TextBox paytext;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantitycolom;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentcolom;
        private System.Windows.Forms.Button export;
    }
}

